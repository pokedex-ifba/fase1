package dev.cancio.finaldex.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import dev.cancio.finaldex.data.model.Pokemon

@Database(entities = [Pokemon::class], version = 2)
abstract class FinaldexDatabase: RoomDatabase() {
    abstract fun finaldexDao(): FinaldexDao
}