package dev.cancio.finaldex.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import dev.cancio.finaldex.data.model.LikeIcon
import dev.cancio.finaldex.data.model.Pokemon

@Dao
interface FinaldexDao {

    @Query("SELECT * FROM pokemon")
    suspend fun getLikedPokemon(): List<Pokemon>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addPokemon(movie: Pokemon)

    @Query("DELETE FROM pokemon WHERE id = :id")
    suspend fun removePokemon(id:Int)

    @Query("SELECT * FROM pokemon WHERE id = :id")
    suspend fun getPokemon(id:Int): Pokemon?

    @Query("SELECT `like` FROM pokemon WHERE id = :id")
    suspend fun getPokemonStatus(id:Int): LikeIcon?
}