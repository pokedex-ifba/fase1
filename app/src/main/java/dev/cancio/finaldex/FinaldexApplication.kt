package dev.cancio.finaldex

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FinaldexApplication : Application()