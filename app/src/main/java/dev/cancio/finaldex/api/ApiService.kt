package dev.cancio.finaldex.api

import dev.cancio.finaldex.data.model.Pokemon
import dev.cancio.finaldex.data.response.PokemonDetailResponse
import dev.cancio.finaldex.data.response.PokemonPagination
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("list/?")
    suspend fun getPokemonPagination(@Query("offset")  offset: Int, @Query("limit") limit: Int) : PokemonPagination

    @GET("detail/{id}")
    suspend fun getPokemon(@Path("id")  id: Int) : Pokemon

    @GET("game/")
    suspend fun getRandomPokemon() : List<Pokemon>
}