package dev.cancio.finaldex.api

import dev.cancio.finaldex.data.model.Pokemon
import dev.cancio.finaldex.data.model.Test
import dev.cancio.finaldex.data.response.PokemonDetailResponse
import dev.cancio.finaldex.data.response.PokemonPagination
import javax.inject.Inject

class PokemonApi @Inject constructor(val api: ApiService) {

    suspend fun getPokemonPagination(offset: Int = 0, limit: Int = 20): PokemonPagination {
        return api.getPokemonPagination(offset, limit)
    }

    suspend fun getPokemon(id: Int): Pokemon {
        return api.getPokemon(id)
    }

    suspend fun getRandomPokemon(): List<Pokemon> {
        return api.getRandomPokemon()
    }
}