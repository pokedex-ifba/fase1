package dev.cancio.finaldex.ui.components

import androidx.compose.animation.Animatable
import androidx.compose.animation.core.LinearOutSlowInEasing
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.keyframes
import androidx.compose.material3.Icon
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import dev.cancio.finaldex.R
import dev.cancio.finaldex.ui.theme.ColorTheme

@Composable
fun PokeProgress(
    modifier: Modifier = Modifier,
    color: Color = ColorTheme.ON_TERTIARY.getColor()
) {
    val animatedColor = remember { Animatable( color) }

    LaunchedEffect(animatedColor) {
        animatedColor.animateTo(
            targetValue = color,
            animationSpec = infiniteRepeatable(
                animation = keyframes {
                    durationMillis = 1000
                    color at 0 with LinearOutSlowInEasing
                    color.copy(alpha = 0.8F) at 200 with LinearOutSlowInEasing
                    color.copy(alpha = 0.4F) at 400 with LinearOutSlowInEasing
                    color.copy(alpha = 0.2F) at 800 with LinearOutSlowInEasing
                }
            )
        )
    }
    Icon(
        painter = painterResource(id = R.drawable.ic_pokehome),
        contentDescription = "loading",
        modifier = modifier,
        tint = animatedColor.value
    )
}

@Preview
@Composable
fun PokePreview() {
    PokeProgress()
}