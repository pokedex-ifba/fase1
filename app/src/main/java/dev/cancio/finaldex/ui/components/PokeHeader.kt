package dev.cancio.finaldex.ui.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import dev.cancio.finaldex.ui.theme.ColorTheme
import dev.cancio.finaldex.ui.theme.ThemeType

@Composable
fun PokeHeader() {
    CenterAlignedTopAppBar(
        title = {
            Text(
                text = "Finaldex",
                style = ThemeType.TITLE.style
            )
        },
        actions = {
            IconButton(onClick = { /* doSomething() */ }) {
                Icon(
                    imageVector = Icons.Filled.AccountCircle,
                    contentDescription = "Localized description",
                    tint = ColorTheme.ON_TERTIARY.getColor()
                )
            }
        }
    )
}
