package dev.cancio.finaldex.ui.components

import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.res.painterResource
import androidx.navigation.NavController
import dev.cancio.finaldex.navigation.BottomNavItem
import dev.cancio.finaldex.ui.theme.ColorTheme

@Composable
fun PokeBar(navController: NavController, itemList: List<BottomNavItem>)  {
    var selectedItem by remember { mutableStateOf(0) }
    NavigationBar(
    ) {
        itemList.forEachIndexed { index, item ->
            NavigationBarItem(
                icon = { Icon(painterResource(id = item.icon), contentDescription = null, tint = ColorTheme.ON_TERTIARY.getColor()) },
                label = { Text(text = item.title, color = ColorTheme.ON_TERTIARY.getColor()) },
                selected = selectedItem == index,
                onClick = {
                    selectedItem = index
                    navController.navigate(item.route)
                }
            )
        }
    }
}