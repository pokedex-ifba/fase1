package dev.cancio.finaldex.ui.screen

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.paging.LoadState
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import dev.cancio.finaldex.ui.components.PokeItem
import dev.cancio.finaldex.ui.components.PokeProgress
import dev.cancio.finaldex.viewmodel.MainViewModel

@Composable
fun HomeScreen(
    navController: NavHostController,
    viewModel: MainViewModel
) {
    val scope = rememberCoroutineScope()
    val pokemonItems = viewModel.getPokemonOnHome(scope).collectAsLazyPagingItems()
    LazyColumn(
        contentPadding = PaddingValues(horizontal = 8.dp),
        verticalArrangement = Arrangement.spacedBy(4.dp),
        modifier = Modifier.fillMaxSize()
    ) {
        items(pokemonItems) {
            if (it != null) {
                PokeItem(it) { viewModel.goToDetail(navController, it.id.toString()) }
            }
        }
        pokemonItems.apply {
            when {
                loadState.refresh is
                        LoadState.Loading -> {
                    item {
                        Box(
                            Modifier
                                .fillMaxSize()
                                .fillMaxHeight()) {
                            PokeProgress(
                                Modifier
                                    .align(
                                        Alignment.Center
                                    )
                                    .padding(top = 100.dp)
                                    .size(150.dp)
                            )
                        }
                    }
                }
                loadState.append is
                        LoadState.Loading -> {
                    item {
                        Box(modifier = Modifier.fillMaxWidth()){
                            PokeProgress(
                                Modifier
                                    .size(50.dp)
                                    .align(Alignment.Center))
                        }
                    }
                }
                loadState.refresh is
                        LoadState.Error -> {
                }
                loadState.append is
                        LoadState.Error -> {
                }
            }
        }
    }
}