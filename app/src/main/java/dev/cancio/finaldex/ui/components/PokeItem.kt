@file:Suppress("OPT_IN_IS_NOT_ENABLED")

package dev.cancio.finaldex.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import dev.cancio.finaldex.data.model.Pokemon
import dev.cancio.finaldex.ui.theme.ColorTheme
import dev.cancio.finaldex.ui.theme.ThemeType

@Composable
fun PokeItem(pokemon: Pokemon, callback: () -> Unit) {

    Box {
        Box(
            modifier = Modifier
                .background(
                    shape = RoundedCornerShape(20.dp),
                    color = ColorTheme.SURFACE_VARIANT.getColor()
                )
                .clickable(onClick = { callback() })
                .padding(8.dp)
                .fillMaxWidth()
                .align(Alignment.Center)
        ) {
            Text(
                text = pokemon.name, style = ThemeType.ITEM_LABEL.style,
                modifier = Modifier
                    .align(Alignment.CenterEnd)
                    .padding(end = 16.dp)
            )
        }
        Box(
            modifier = Modifier
                .padding(start = 8.dp)
                .width(70.dp)
                .height(70.dp)
                .background(
                    shape = RoundedCornerShape(40.dp),
                    brush = Brush.verticalGradient(
                        colors = listOf(
                            Color.Transparent,
                            Color.Transparent,
                            Color.Transparent,
                            pokemon.colorDetail.colorTheme
                                .getColor()
                                .copy(alpha = 0.2F),
                            pokemon.colorDetail.colorTheme.getColor()
                        )
                    )
                )
        ) {
            ImageWeb(
                url = pokemon.avatarUrl,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(4.dp)
                    .align(Alignment.Center)
            )
        }
    }
}

@Preview
@Composable
fun PokeItemPreview() {
    //val pokemon = PokemonRepository().getAllPokemon().first()
    //PokeItem(pokemon){}
}