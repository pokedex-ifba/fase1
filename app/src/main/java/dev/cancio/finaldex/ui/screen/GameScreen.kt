package dev.cancio.finaldex.ui.screen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.unit.dp
import dev.cancio.finaldex.data.enum.ColorDetail
import dev.cancio.finaldex.data.model.Pokemon
import dev.cancio.finaldex.ui.components.ImageWeb
import dev.cancio.finaldex.ui.theme.ColorTheme
import dev.cancio.finaldex.viewmodel.GameViewModel

@Composable
fun GameScreen(viewModel: GameViewModel) {
    val scope = rememberCoroutineScope()
    val pokemonList = remember { mutableStateOf(listOf<Pokemon>()) }
    val response = remember { mutableStateOf(Result.NONE) }
    val enabledButton = remember { mutableStateOf(true) }
    val rightPokemon = remember {
        mutableStateOf(
            Pokemon(
                id = 2,
                colorDetail = ColorDetail.BLACK
            )
        )
    }
    if (pokemonList.value.isEmpty()) {
        pokemonList.value = viewModel.getRandomPokemonList(scope)
        if (pokemonList.value.isNotEmpty()) rightPokemon.value = pokemonList.value.first()
    }

    //Screen
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .fillMaxSize()
            .background(
                color = ColorTheme.TERTIARY.getColor()
            )
    ) {
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.Center),
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            item {
                ImageWeb(
                    url = rightPokemon.value.avatarUrl,
                    colorFilter = when (response.value) {
                        Result.NONE -> ColorFilter.lighting(Color.Black, Color.Black)
                        else -> null
                    },
                    modifier = Modifier
                        .size(200.dp)
                        .background(
                            brush = Brush.radialGradient(
                                colors = listOf(
                                    ColorTheme.WHITE.getColor(),
                                    ColorTheme.BLUE.getColor(),
                                    Color.Transparent,
                                )
                            )
                        )
                        .padding(36.dp)
                )
            }
            item {
                Text(
                    when (response.value) {
                        Result.NONE -> "Who's that pokemon?"
                        Result.RIGHT -> "Acertou! É o ${rightPokemon.value.name}"
                        Result.WRONG -> "Errou! É o ${rightPokemon.value.name}"
                    }
                )
            }
            items(pokemonList.value) {
                Button(
                    onClick = {
                        if (enabledButton.value) {
                            response.value = getResponse(it, rightPokemon.value)
                            enabledButton.value = false
                        }
                    },
                    enabled = enabledButton.value
                ) {
                    Text(text = it.name)
                }
            }
        }
    }
}

fun getResponse(pokemon: Pokemon, rightPokemon: Pokemon) = when (pokemon) {
    rightPokemon -> Result.RIGHT
    else -> Result.WRONG
}

enum class Result {
    WRONG,
    RIGHT,
    NONE
}