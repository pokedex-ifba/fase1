package dev.cancio.finaldex.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import dev.cancio.finaldex.R

val ubuntu = FontFamily(
    Font(R.font.ubuntubold, weight = FontWeight.Bold, style = FontStyle.Normal),
    Font(R.font.ubuntubolditalic, weight = FontWeight.Bold, style = FontStyle.Italic),
    Font(R.font.ubuntuitalic, weight = FontWeight.Normal, style = FontStyle.Italic),
    Font(R.font.ubuntulight, weight = FontWeight.Light, style = FontStyle.Normal),
    Font(R.font.ubuntulightitalic, weight = FontWeight.Light, style = FontStyle.Italic),
    Font(R.font.ubuntumedium, weight = FontWeight.Medium, style = FontStyle.Normal),
    Font(R.font.ubuntumediumitalic, weight = FontWeight.Medium, style = FontStyle.Italic),
    Font(R.font.ubunturegular, weight = FontWeight.Normal, style = FontStyle.Normal),
)
val righteous = FontFamily(
    Font(R.font.righteous)
)

enum class ThemeType(val style: TextStyle){
    TITLE(TextStyle(
        fontFamily = righteous,
        fontWeight = FontWeight.Normal,
        fontSize = 22.sp,
        lineHeight = 28.sp,
        letterSpacing = 0.sp
    )),
    TEXT(TextStyle(
        fontFamily = ubuntu,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp,
        lineHeight = 24.sp,
        letterSpacing = 0.5.sp
    )),
    ITEM_LABEL(TextStyle(
        fontFamily = ubuntu,
        fontWeight = FontWeight.Bold,
        fontSize = 20.sp,
        lineHeight = 24.sp,
        letterSpacing = 0.5.sp
    ));

    companion object{
        fun typography() = Typography(
            bodyLarge = TITLE.style,
            titleLarge = TEXT.style,
        )
    }

}