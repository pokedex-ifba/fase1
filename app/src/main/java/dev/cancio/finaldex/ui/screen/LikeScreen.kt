package dev.cancio.finaldex.ui.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import dev.cancio.finaldex.ui.components.PokeItem
import dev.cancio.finaldex.viewmodel.LikeViewModel

@Composable
fun LikeScreen(
    navController: NavHostController,
    viewModel: LikeViewModel
) {
    val scope = rememberCoroutineScope()
    val pokemonItems = viewModel.getPokemonOnDetail(scope).collectAsLazyPagingItems()
        LazyColumn(
        contentPadding = PaddingValues(horizontal = 8.dp),
        verticalArrangement = Arrangement.spacedBy(4.dp),
        modifier = Modifier.fillMaxSize()
    ) {
        items(pokemonItems){
            if (it != null){
                PokeItem(it) { viewModel.goToDetail(navController,it.id.toString()) }
            }
        }
    }
}