package dev.cancio.finaldex.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

enum class ColorTheme(
    val darkColor: Color,
    val lightColor: Color
){
    PRIMARY(Color(0xFF242943),Color(0xFFFFE77C)), //ok
    ON_PRIMARY(Color(0xFF9FA8D3),Color(0xFFFFFCEF)),
    PRIMARY_CONTAINER(Color(0xFF1C2035),Color(0xFFFFDF52)), //ok
    ON_PRIMARY_CONTAINER(Color(0xFF16161F),Color(0xFFB99F2A)), //ok
    SECONDARY(Color(0xFFAF2D30),Color(0xFFFFB3AE)),
    ON_SECONDARY(Color(0xFFFFFFFF),Color(0xFF68000C)),
    SECONDARY_CONTAINER(Color(0xFFFFDAD7),Color(0xFF8D131B)),
    ON_SECONDARY_CONTAINER(Color(0xFF410005),Color(0xFFFFDAD7)),
    TERTIARY(Color(0xFF6F465D),Color(0xFFF9C3A0)), //ok
    ON_TERTIARY(Color(0xFFF35F5C),Color(0xFFF35F5C)), //ok
    TERTIARY_CONTAINER(Color(0xFFDEE1FF),Color(0xFF2E4090)),
    ON_TERTIARY_CONTAINER(Color(0xFF001258),Color(0xFFDEE1FF)),
    ERROR(Color(0xFFBA1A1A),Color(0xFFFFB4AB)),
    ERROR_CONTAINER(Color(0xFFFFDAD6),Color(0xFF93000A)),
    ON_ERROR(Color(0xFFFFFFFF),Color(0xFF690005)),
    ON_ERROR_CONTAINER(Color(0xFF410002),Color(0xFFFFDAD6)),
    BACKGROUND(Color(0xFF1C2035),Color(0xFFFFFCEF)),
    ON_BACKGROUND(Color(0xFFB99F2A),Color(0xFF9FA8D3)),
    SURFACE(Color(0xFF1C2035),Color(0xFFFFF9DC)),//ok
    ON_SURFACE(Color(0xFFF9C3A0),Color(0xFF6F465D)),//ok
    SURFACE_VARIANT(Color(0xFF232A4D),Color(0xFFFFF2B7)), //ok
    ON_SURFACE_VARIANT(Color(0xFFF35F5C),Color(0xFFF35F5C)), //ok
    OUTLINE(Color(0xFF7C7768),Color(0xFF969080)),
    INVERSE_ON_SURFACE(Color(0xFFF6F0E7),Color(0xFF1D1B16)),
    INVERSE_SURFACE(Color(0xFF32302A),Color(0xFFE7E2D9)),
    INVERSE_PRIMARY(Color(0xFFDFC64C),Color(0xFF6E5E00)),
    SHADOW(Color(0xFF000000),Color(0xFF000000)),
    SURFACE_TINT(Color(0xFF6E5E00),Color(0xFFDFC64C)),
    BLACK(Color(0xFF9C9C9C),Color(0xFF9C9C9C)),//ok
    BLUE(Color(0xFF69B7FF),Color(0xFF69B7FF)), //ok
    BROWN(Color(0xFFD68270),Color(0xFFD68270)),//ok
    GRAY(Color(0xFFBFBFBF),Color(0xFFBFBFBF)),//ok
    GREEN(Color(0xFF51FF96),Color(0xFF51FF96)),//ok
    PINK(Color(0xFFFF69C3),Color(0xFFFF69C3)),//ok
    PURPLE(Color(0xFF9E779E),Color(0xFF9E779E)),//ok
    RED(Color(0xFFFF6969),Color(0xFFFF6969)),//ok
    WHITE(Color(0xFFEAEAEA),Color(0xFFEAEAEA)),
    YELLOW(Color(0xFFFFD465),Color(0xFFFFD465));

    @Composable
    fun getColor() = if(isSystemInDarkTheme()) this.darkColor else this.lightColor

    companion object {
        fun getLightColorScheme() = darkColorScheme(
            primary = PRIMARY.lightColor,
            onPrimary = ON_PRIMARY.lightColor,
            primaryContainer = PRIMARY_CONTAINER.lightColor,
            onPrimaryContainer = ON_PRIMARY_CONTAINER.lightColor,
            secondary = SECONDARY_CONTAINER.lightColor,
            onSecondary = ON_SECONDARY_CONTAINER.lightColor,
            secondaryContainer = TERTIARY.lightColor,
            onSecondaryContainer = ON_TERTIARY.lightColor,
            tertiary = TERTIARY_CONTAINER.lightColor,
            onTertiary = ON_TERTIARY_CONTAINER.lightColor,
            tertiaryContainer = ERROR.lightColor,
            onTertiaryContainer = ERROR_CONTAINER.lightColor,
            error = ON_ERROR.lightColor,
            errorContainer = ON_ERROR_CONTAINER.lightColor,
            onError = BACKGROUND.lightColor,
            onErrorContainer = ON_BACKGROUND.lightColor,
            background = SURFACE.lightColor,
            onBackground = ON_SURFACE.lightColor,
            surface = SURFACE_VARIANT.lightColor,
            onSurface = ON_SURFACE_VARIANT.lightColor,
            surfaceVariant = OUTLINE.lightColor,
            onSurfaceVariant = INVERSE_ON_SURFACE.lightColor,
            outline = INVERSE_SURFACE.lightColor,
            inverseOnSurface = INVERSE_PRIMARY.lightColor,
            inverseSurface = SHADOW.lightColor,
            inversePrimary = SURFACE_TINT.lightColor,
        )
        fun getDarkColorScheme() = darkColorScheme(
            primary = PRIMARY.darkColor,
            onPrimary = ON_PRIMARY.darkColor,
            primaryContainer = PRIMARY_CONTAINER.darkColor,
            onPrimaryContainer = ON_PRIMARY_CONTAINER.darkColor,
            secondary = SECONDARY_CONTAINER.darkColor,
            onSecondary = ON_SECONDARY_CONTAINER.darkColor,
            secondaryContainer = TERTIARY.darkColor,
            onSecondaryContainer = ON_TERTIARY.darkColor,
            tertiary = TERTIARY_CONTAINER.darkColor,
            onTertiary = ON_TERTIARY_CONTAINER.darkColor,
            tertiaryContainer = ERROR.darkColor,
            onTertiaryContainer = ERROR_CONTAINER.darkColor,
            error = ON_ERROR.darkColor,
            errorContainer = ON_ERROR_CONTAINER.darkColor,
            onError = BACKGROUND.darkColor,
            onErrorContainer = ON_BACKGROUND.darkColor,
            background = SURFACE.darkColor,
            onBackground = ON_SURFACE.darkColor,
            surface = SURFACE_VARIANT.darkColor,
            onSurface = ON_SURFACE_VARIANT.darkColor,
            surfaceVariant = OUTLINE.darkColor,
            onSurfaceVariant = INVERSE_ON_SURFACE.darkColor,
            outline = INVERSE_SURFACE.darkColor,
            inverseOnSurface = INVERSE_PRIMARY.darkColor,
            inverseSurface = SHADOW.darkColor,
            inversePrimary = SURFACE_TINT.darkColor,
        )
    }
}