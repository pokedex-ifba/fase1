package dev.cancio.finaldex.ui.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Icon
import androidx.compose.material3.SmallFloatingActionButton
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.cancio.finaldex.ui.components.ImageWeb
import dev.cancio.finaldex.ui.components.PokeValues
import dev.cancio.finaldex.viewmodel.DetailViewModel
import kotlinx.coroutines.launch

@Composable
fun DetailScreen(pokemonId: String,viewModel: DetailViewModel) {
    val scope = rememberCoroutineScope()
    val pokemon = viewModel.getPokemon(pokemonId,scope)
    val iconItem = remember { mutableStateOf(pokemon.like.getIcon()) }

    iconItem.value = pokemon.like.getIcon()

    Box(
        contentAlignment = Alignment.Center
    ){
        Column (
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ){
            ImageWeb(url = pokemon.avatarUrl)
            Text(text = pokemon.name)
            SmallFloatingActionButton(
                onClick = {
                    scope.launch {
                        iconItem.value = viewModel.updatePokemon(pokemon)
                    }
                },
            ) {
                Icon(iconItem.value, contentDescription = "Localized description")
            }
            PokeValues("Força")
            PokeValues("Velocidade")
            PokeValues("Habilidade")
            PokeValues("Dano")
        }
    }
}