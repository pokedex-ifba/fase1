package dev.cancio.finaldex.ui.components

import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.foundation.layout.Box
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.foundation.Image
import coil.annotation.ExperimentalCoilApi
import coil.compose.ImagePainter
import coil.compose.rememberImagePainter

@OptIn(ExperimentalCoilApi::class)
@Composable
fun ImageWeb(
    url: String,
    modifier: Modifier = Modifier,
    colorFilter: ColorFilter? = null
) {
    val painter = rememberImagePainter(
        data = url,
        builder = {
            crossfade(true)
        }
    )
    Box{
        Image(
            painter = painter,
            contentDescription = "",
            modifier = modifier,
            colorFilter = colorFilter
        )
        when (painter.state) {
            is ImagePainter.State.Loading -> {
                PokeProgress(Modifier.align(Alignment.Center))
            }
            is ImagePainter.State.Error -> {
            }
        }
    }
}