package dev.cancio.finaldex.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dev.cancio.finaldex.api.*
import dev.cancio.finaldex.dao.FinaldexDao
import dev.cancio.finaldex.dao.FinaldexDatabase
import dev.cancio.finaldex.repository.PokemonRepository
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class FinaldexModule {

    companion object {
        const val POKE_API = "https://pokeapi.co/api/v2/"
        const val FINALDEX_API = "http://10.0.2.2:3000"
    }

    @Singleton
    @Provides
    @Named("Normal")
    fun provideRetrofit(): Retrofit = Retrofit
        .Builder()
        .baseUrl(FINALDEX_API)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Singleton
    @Provides
    fun provideService(@Named("Normal") retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Singleton
    @Provides
    fun providesApi(apiService: ApiService) = PokemonApi(apiService)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context) = Room
        .databaseBuilder(context, FinaldexDatabase::class.java, "finaldex-db")
        .allowMainThreadQueries()
        .fallbackToDestructiveMigration()
        .build()

    @Singleton
    @Provides
    fun provideDao(database: FinaldexDatabase) = database.finaldexDao()

    @Singleton
    @Provides
    fun providesRepository(api: PokemonApi, dao: FinaldexDao) = PokemonRepository(api, dao)


}