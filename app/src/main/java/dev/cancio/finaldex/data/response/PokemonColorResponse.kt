package dev.cancio.finaldex.data.response

import androidx.compose.runtime.Composable
import com.google.gson.annotations.SerializedName
import dev.cancio.finaldex.data.enum.ColorDetail
import java.io.Serializable

data class PokemonColorResponse(
    @SerializedName("name")
    val colorDetail: ColorDetail,
    val url: String = ""
): Serializable {
    @Composable
    fun getColor() = colorDetail.colorTheme.getColor()
}