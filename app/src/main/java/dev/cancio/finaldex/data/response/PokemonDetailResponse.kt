package dev.cancio.finaldex.data.response

import dev.cancio.finaldex.data.model.LikeIcon
import dev.cancio.finaldex.data.model.Pokemon
import java.io.Serializable

data class PokemonDetailResponse(
    val name: String,
    val id: Int,
    val color: PokemonColorResponse
) : Serializable {
}