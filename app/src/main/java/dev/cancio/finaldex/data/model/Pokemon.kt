package dev.cancio.finaldex.data.model

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.room.*
import dev.cancio.finaldex.data.enum.ColorDetail
import dev.cancio.finaldex.repository.PokemonRepository

@Entity
data class Pokemon(
    @PrimaryKey
    var id: Int = 0,
    var name: String = "who that's pokemon",
    var colorDetail: ColorDetail = ColorDetail.BLACK,
    var avatarUrl: String = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/0.png",
    @ColumnInfo(name = "like")
    @TypeConverters(LikeIconConverter::class)
    var like: LikeIcon = LikeIcon.LIKE
)


enum class LikeIcon(val value: Int) {
    LIKE(0),
    UNLIKE(1);

    fun getIcon() = when(this){
        LIKE -> Icons.Outlined.Favorite
        UNLIKE -> Icons.Outlined.FavoriteBorder
    }
}

class LikeIconConverter{
    @TypeConverter
    fun fromLikeIcon(value: LikeIcon) = value.name

    @TypeConverter
    fun toLikeIcon(value: String) =  enumValueOf<LikeIcon>(value)
}