package dev.cancio.finaldex.data.enum

import com.google.gson.annotations.SerializedName
import dev.cancio.finaldex.ui.theme.ColorTheme

enum class ColorDetail(val colorTheme: ColorTheme) {
    @SerializedName("black")
    BLACK(ColorTheme.BLACK),

    @SerializedName("blue")
    BLUE(ColorTheme.BLUE),

    @SerializedName("brown")
    BROWN(ColorTheme.BROWN),

    @SerializedName("gray")
    GRAY(ColorTheme.GRAY),

    @SerializedName("green")
    GREEN(ColorTheme.GREEN),

    @SerializedName("pink")
    PINK(ColorTheme.PINK),

    @SerializedName("purple")
    PURPLE(ColorTheme.PURPLE),

    @SerializedName("red")
    RED(ColorTheme.RED),

    @SerializedName("white")
    WHITE(ColorTheme.WHITE),

    @SerializedName("yellow")
    YELLOW(ColorTheme.YELLOW);
}