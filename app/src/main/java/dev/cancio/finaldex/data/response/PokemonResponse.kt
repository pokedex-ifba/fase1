package dev.cancio.finaldex.data.response

import dev.cancio.finaldex.data.enum.ColorDetail
import dev.cancio.finaldex.data.model.Pokemon

data class PokemonResponse(
    val name: String,
    val url: String,
){
    val id: Int
        get() = url
            .removePrefix("https://pokeapi.co/api/v2/pokemon-species/")
            .removeSuffix("/")
            .toInt()
}
