package dev.cancio.finaldex.navigation

import androidx.annotation.DrawableRes
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import dev.cancio.finaldex.R
import dev.cancio.finaldex.ui.screen.DetailScreen
import dev.cancio.finaldex.ui.screen.GameScreen
import dev.cancio.finaldex.ui.screen.HomeScreen
import dev.cancio.finaldex.ui.screen.LikeScreen
import dev.cancio.finaldex.viewmodel.DetailViewModel
import dev.cancio.finaldex.viewmodel.GameViewModel
import dev.cancio.finaldex.viewmodel.LikeViewModel
import dev.cancio.finaldex.viewmodel.MainViewModel

@Composable
fun MainNavigation(navController: NavHostController) {

    val mainViewModel = hiltViewModel<MainViewModel>()
    val gameViewModel = hiltViewModel<GameViewModel>()
    val detailViewModel = hiltViewModel<DetailViewModel>()
    val likeViewModel = hiltViewModel<LikeViewModel>()

    NavHost(navController = navController, startDestination = "Home") {
        composable("Home")  {  HomeScreen(navController, mainViewModel) }
        composable("Likes")  {  LikeScreen(navController,likeViewModel) }
        composable(
            "Detail/{pokemonId}",
            arguments = listOf(navArgument("pokemonId") { type = NavType.StringType })
        ) { backStackEntry ->
            backStackEntry.arguments?.getString("pokemonId")?.let { DetailScreen(it,detailViewModel) }
        }
        composable("Game")  {  GameScreen(gameViewModel) }
    }
}

sealed class BottomNavItem(
    val route: String,
    @DrawableRes val icon: Int,
    val title: String
) {
    object Home : BottomNavItem("home", R.drawable.ic_pokehome, "Home")
    object Likes : BottomNavItem("likes", R.drawable.ic_pokestar, "Likes")
    object Game : BottomNavItem("game", R.drawable.ic_pokegame, "Game")
}

sealed class PokedexRoutes(val itemList: List<BottomNavItem>){
    object MainRoute: PokedexRoutes(listOf(BottomNavItem.Home, BottomNavItem.Likes, BottomNavItem.Game))
}