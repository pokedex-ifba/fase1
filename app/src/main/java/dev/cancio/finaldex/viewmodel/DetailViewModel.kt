package dev.cancio.finaldex.viewmodel

import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.cancio.finaldex.data.enum.ColorDetail
import dev.cancio.finaldex.data.model.Pokemon
import dev.cancio.finaldex.repository.PokemonRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(private val repository: PokemonRepository) : ViewModel()  {
    private var detail = mutableStateOf(Pokemon(colorDetail = ColorDetail.BLACK))

    fun getPokemon(pokemonId: String, scope: CoroutineScope): Pokemon{
        scope.launch {
            detail.value = repository.getPokemonDetail(pokemonId)
        }
        return detail.value
    }

    suspend fun updatePokemon(pokemon: Pokemon): ImageVector {
        return repository.updatePokemon(pokemon)
    }
}