package dev.cancio.finaldex.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.paging.PagingData
import androidx.paging.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.cancio.finaldex.data.model.Pokemon
import dev.cancio.finaldex.data.model.Test
import dev.cancio.finaldex.repository.PokemonRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: PokemonRepository)  : ViewModel() {

    var uiState by mutableStateOf(Test(""))

    fun getPokemonOnHome(scope: CoroutineScope): Flow<PagingData<Pokemon>> {
        return repository.getPokemonPagination().cachedIn(scope)
    }

    fun goToDetail(navController: NavController, id: String) {
        navController.navigate("detail/${id}")
    }
}