package dev.cancio.finaldex.viewmodel

import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.paging.PagingData
import androidx.paging.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.cancio.finaldex.data.model.Pokemon
import dev.cancio.finaldex.repository.PokemonRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class LikeViewModel @Inject constructor(private val repository: PokemonRepository) : ViewModel()  {
    fun getPokemonOnDetail(scope: CoroutineScope): Flow<PagingData<Pokemon>> {
        return repository.getLikePokemon().cachedIn(scope)
    }

    fun goToDetail(navController: NavController, id: String) {
        navController.navigate("detail/${id}")
    }
}