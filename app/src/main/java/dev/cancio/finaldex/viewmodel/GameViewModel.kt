package dev.cancio.finaldex.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.cancio.finaldex.data.model.Pokemon
import dev.cancio.finaldex.repository.PokemonRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GameViewModel @Inject constructor(private val repository: PokemonRepository)  : ViewModel() {
    private var pokemonList: List<Pokemon> = listOf()

    fun getRandomPokemonList(scope: CoroutineScope): List<Pokemon> {
        scope.launch {
            pokemonList = repository.getRandomPokemonList()
        }
        return pokemonList
    }
}