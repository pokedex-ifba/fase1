package dev.cancio.finaldex.repository

import android.util.Log
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import dev.cancio.finaldex.api.PokemonApi
import dev.cancio.finaldex.dao.FinaldexDao
import dev.cancio.finaldex.data.enum.ColorDetail
import dev.cancio.finaldex.data.model.LikeIcon
import dev.cancio.finaldex.data.model.Pokemon
import dev.cancio.finaldex.data.model.Test
import dev.cancio.finaldex.pagination.DetailDataSource
import dev.cancio.finaldex.pagination.MainDataSource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class PokemonRepository @Inject constructor(
    private val api: PokemonApi,
    private val dao: FinaldexDao
) {

    suspend fun updatePokemon(pokemon: Pokemon): ImageVector {
        val hasPokemon = dao.getPokemon(pokemon.id) != null

        return if (hasPokemon) {
            dao.removePokemon(pokemon.id)
            Icons.Outlined.FavoriteBorder
        } else {
            dao.addPokemon(pokemon)
            Icons.Outlined.Favorite
        }
    }

    fun getPokemonPagination(offset: Int = 0, limit: Int = 20): Flow<PagingData<Pokemon>> {
        return Pager(
            config = PagingConfig(pageSize = 20),
            pagingSourceFactory = { MainDataSource(api) }
        ).flow
    }

    fun getLikePokemon(): Flow<PagingData<Pokemon>> {
        return Pager(
            config = PagingConfig(pageSize = 20),
            pagingSourceFactory = { DetailDataSource(dao) }
        ).flow
    }

    suspend fun getPokemonDetail(pokemonId: String): Pokemon {
        val like = dao.getPokemonStatus(pokemonId.toInt()) ?: LikeIcon.UNLIKE
        return api.getPokemon(pokemonId.toInt())
    }

    suspend fun getRandomPokemonList(): List<Pokemon> {
        val result = api.getPokemonPagination().results
        val randomList = (0..19).shuffled()
        val response = listOf(
            result[randomList[0]],
            result[randomList[1]],
            result[randomList[2]]
        )
        Log.e("response", response.toString())
        return response
    }
}