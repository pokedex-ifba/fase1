package dev.cancio.finaldex.pagination

import androidx.paging.PagingSource
import androidx.paging.PagingState
import dev.cancio.finaldex.dao.FinaldexDao
import dev.cancio.finaldex.data.model.Pokemon

class DetailDataSource(
    private val dao: FinaldexDao
): PagingSource<Int,Pokemon>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Pokemon> {
        return try {

            val response = dao.getLikedPokemon()

            LoadResult.Page(
                data = response,
                prevKey = null,
                nextKey = null
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Pokemon>): Int? {
        TODO("Not yet implemented")
    }
}