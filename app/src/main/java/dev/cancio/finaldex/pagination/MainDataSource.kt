package dev.cancio.finaldex.pagination

import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingSource
import androidx.paging.PagingState
import dev.cancio.finaldex.api.PokemonApi
import dev.cancio.finaldex.data.model.Pokemon

class MainDataSource(
    private val api: PokemonApi
) : PagingSource<Int, Pokemon>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Pokemon> {
        return try {
            val nextPageNumber = params.key ?: 0
            val response = api.getPokemonPagination(nextPageNumber)
            val pokemonList = response.results
            with(response) {
                LoadResult.Page(
                    data = pokemonList,
                    prevKey = null,
                    nextKey = results.lastOrNull()?.id?.plus(1)
                )
            }
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    @ExperimentalPagingApi
    override fun getRefreshKey(state: PagingState<Int, Pokemon>): Int? {
        return state.anchorPosition?.let {
            state.closestPageToPosition(it)?.prevKey?.plus(20)
                ?: state.closestPageToPosition(it)?.nextKey?.minus(20)
        }
    }

    private fun String?.getOffset(): Int? = this
        ?.removePrefix("https://pokeapi.co/api/v2/pokemon/?offset=")
        ?.removeSuffix("&limit=20")
        ?.toInt()
}
